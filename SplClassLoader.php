<?php
/**
 * SplClassLoader implementation that implements the technical interoperability
 * standards for PHP 5.3 namespaces and class names.
 *
 *     // Example which loads classes for the Doctrine Common package in the
 *     // Doctrine\Common namespace.
 *     $classLoader = new SplClassLoader('Doctrine\Common');
 *     $classLoader->register();
 */
class SplClassLoader
{
    private $_fileExtension = '.php';
    private $_namespace;
    private $_includePath;
    private $_namespaceSeparator = '\\';

    /**
     * Creates a new <tt>SplClassLoader</tt> that loads classes of the
     * specified namespace.
     *
     * @param string $ns The namespace to use.
     */
    public function __construct($ns)
    {
        $this->_namespace = $ns;
    }

    /**
     * Sets the namespace separator used by classes in the namespace of this class loader.
     *
     * @param string $sep The separator to use.
     */
    public function setNamespaceSeparator($sep)
    {
        $this->_namespaceSeparator = $sep;
    }

    /**
     * Sets the base include path for all class files in the namespace of this class loader.
     *
     * @param string $basePath
     */
    public function setIncludePath($includePath)
    {
        $this->_includePath = $includePath;
    }

    /**
     * Sets the file extension of class files in the namespace of this class loader.
     *
     * @param string $fileExtension
     */
    public function setFileExtension($fileExtension)
    {
        $this->_fileExtension = $fileExtension;
    }

    /**
     * Installs this class loader on the SPL autoload stack.
     */
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    /**
     * Loads the given class or interface.
     *
     * @param string $classname The name of the class to load.
     * @return boolean TRUE if the class has been successfully loaded, FALSE otherwise.
     */
    public function loadClass($className)
    {
        if (strpos($className, $this->_namespace.$this->_namespaceSeparator) !== 0) {
            return false;
        }

        // David custom code: Sense això, el PHP 7 no sap fer un Exception del require i no mostra 404
        $fileName = ($this->_includePath !== null ? $this->_includePath . DIRECTORY_SEPARATOR : '')
            . str_replace($this->_namespaceSeparator, DIRECTORY_SEPARATOR, $className)
            . $this->_fileExtension;
        if (!file_exists($fileName))
            throw new Exception("Class not found");

        require $fileName;

        return true;
    }
}