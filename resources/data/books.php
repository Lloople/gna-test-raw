<?php
return [
    [
        'id'           => 1,
        'name'         => 'Un mundo feliz',
        'price'        => 20.00,
        'author_id'    => 1,
        'publisher_id' => 1,
    ],
    [
        'id'           => 2,
        'name'         => 'La isla',
        'price'        => 20.00,
        'author_id'    => 1,
        'publisher_id' => 2,
    ],
    [
        'id'           => 3,
        'name'         => 'Time Must Have a Stop',
        'price'        => 20.00,
        'author_id'    => 1,
        'publisher_id' => 3,
    ],
    [
        'id'           => 4,
        'name'         => 'The Genius and the Goddess',
        'price'        => 20.00,
        'author_id'    => 1,
        'publisher_id' => 4,
    ],
    [
        'id'           => 5,
        'name'         => 'Crome Yellow',
        'price'        => 20.00,
        'author_id'    => 1,
        'publisher_id' => 1,
    ],

    [
        'id'           => 6,
        'name'         => 'Cançó de gel i foc',
        'price'        => 10.50,
        'author_id'    => 2,
        'publisher_id' => 4,
    ],
    [
        'id'           => 7,
        'name'         => 'Xoc de reis',
        'price'        => 12.50,
        'author_id'    => 2,
        'publisher_id' => 4,
    ],
    [
        'id'           => 8,
        'name'         => 'Dança amb dracs',
        'price'        => 15.50,
        'author_id'    => 2,
        'publisher_id' => 4,
    ],
    [
        'id'           => 9,
        'name'         => 'Tempesta d\'espases',
        'price'        => 11.50,
        'author_id'    => 2,
        'publisher_id' => 4,
    ],

    [
        'id'           => 10,
        'name'         => 'Muerte de la luz',
        'price'        => 9.50,
        'author_id'    => 2,
        'publisher_id' => 3,
    ],
    [
        'id'           => 11,
        'name'         => 'Sueño del Fevre',
        'price'        => 5.00,
        'author_id'    => 2,
        'publisher_id' => 5,
    ],
];