<?php

return [
    [
        'id'   => 1,
        'name' => 'Planeta',
    ],
    [
        'id'   => 2,
        'name' => 'Agostini',
    ],
    [
        'id'   => 3,
        'name' => 'Lubna',
    ],
    [
        'id'   => 4,
        'name' => 'Gigamesh',
    ],
    [
        'id'   => 5,
        'name' => 'Virtual',
    ],
];