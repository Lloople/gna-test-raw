<?php
include("start_app.php");


const DEFAULT_URL = "books/list";
//dd($_SERVER);
if (array_key_exists('PATH_INFO', $_SERVER)) {
    $path = $_SERVER['PATH_INFO'];
} else {
    $path = $_SERVER['REQUEST_URI'];
}

$path = preg_replace('/(.*)(\?.*)/i', '${1}', $path);
define('REQUEST_PATH', $path); // Ens el guardem per poder fer-lo servir al header pel menú
$pathPieces = array_values(array_filter(explode("/", $path)));


/*
 * A partir d'una ruta tipus controlador/metode, decodificar-la
 * per poder executar el mètode. Per exemple:
 * books/list => BooksController->getList()
 */
if (count($pathPieces) > 1) {
    $controllerName = ucfirst($pathPieces[0]);
    $method = $pathPieces[1];
} elseif (count($pathPieces) == 1) {
    $controllerName = "General";
    $method = $pathPieces[0];
} else {
    return redirect(DEFAULT_URL);
}

// Transformar kebab-case a camelCase
$methodPieces = explode('-', $method);
$methodPieces = array_map(function ($piece) { return ucfirst($piece); }, $methodPieces);
$method = "get" . implode('', $methodPieces);

$controllerClassName = "Gna\\Controllers\\" . $controllerName . "Controller";

try {
    $controllerInstance = new $controllerClassName();
} catch (Exception $e) {
    die("404 Not Found");
}

if (!method_exists($controllerInstance, $method)) {
    die("404 Not Found");
}

return $controllerInstance->$method();
