<?php
// Preparar els errors
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// Carregar les classes per Namespaces
include('SplClassLoader.php');
$loader = new SplClassLoader("Gna", __DIR__);
$loader->register();


// Funcions d'ajuda
include('helpers.php');