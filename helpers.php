<?php

function redirect($path)
{
    header("Location: " . $path);
}

/*
 * Per debugar on-the-fly
 */
function dd($var)
{
    echo "<pre>";
    print_r($var);
    exit;
}