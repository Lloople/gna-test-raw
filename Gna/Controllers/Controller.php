<?php

namespace Gna\Controllers;

use Gna\Helpers\Request;

class Controller
{

    private $request;

    public function __construct()
    {

        $this->request = new Request();
    }

    public function request()
    {

        return $this->request;
    }

}