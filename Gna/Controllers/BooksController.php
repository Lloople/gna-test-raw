<?php

namespace Gna\Controllers;


use Gna\Repositories\BooksRepository;

class BooksController extends Controller
{

    private $booksRepository;

    public function __construct()
    {

        $this->booksRepository = new BooksRepository();
        parent::__construct();
    }

    /*
     * /books/list
     * Llistat de llibres
     */
    public function getList()
    {

        $inputAuthor = $this->request()->input('author');
        $inputPrice = $this->request()->input('price');
        $inputName = $this->request()->input('name');

        $books = $this->booksRepository->getBooksByAuthorPriceName($inputAuthor, $inputPrice, $inputName);
        $orderByAuthor = $this->request()->input('order_by_author');

        if (!is_null($orderByAuthor)) {
            $books = $this->booksRepository->orderBooksByAuthor($books, $orderByAuthor);
        }


        return include ('Gna/Views/books-list.php');
    }
}