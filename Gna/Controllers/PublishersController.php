<?php

namespace Gna\Controllers;


use Gna\Repositories\PublishersRepository;

class PublishersController extends Controller
{

    private $publishersRepository;

    public function __construct()
    {

        $this->publishersRepository = new PublishersRepository();
        parent::__construct();
    }

    /*
     * /publishers/list
     * Llistat d'editorials
     */
    public function getList()
    {

        $inputAuthor = $this->request()->input('author');
        $inputName = $this->request()->input('name');

        $publishers = $this->publishersRepository->getPublishersByAuthorName($inputAuthor, $inputName);


        return include ('Gna/Views/publishers-list.php');
    }
}