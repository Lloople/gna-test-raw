<?php

namespace Gna\Helpers;

/*
 * Simulem una base de dades
 */
class Database
{

    private $data = [];

    public function __construct()
    {

        $this->data = [
            'books'      => include('resources/data/books.php'),
            'authors'    => include('resources/data/authors.php'),
            'publishers' => include('resources/data/publishers.php'),
        ];
    }

    public function get($table)
    {

        if (array_key_exists($table, $this->data)) {
            return $this->data[$table];
        }

        return null;

    }

}