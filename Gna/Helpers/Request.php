<?php

namespace Gna\Helpers;

/*
 * Guardar els inputs que arriben per GET i accedir des d'aquí
 *
 */
class Request
{

    private $fields;

    public function __construct()
    {

        $this->fields = (object)$_GET;
    }

    public function getFields()
    {

        return $this->fields;
    }

    public function input($input, $fallback = null)
    {

        return isset($this->fields->{$input}) && $this->fields->{$input} !== '' ? $this->fields->{$input} : $fallback;
    }
}