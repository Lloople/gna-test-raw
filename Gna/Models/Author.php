<?php

namespace Gna\Models;

use Gna\Models\Parents\Fillable;

class Author extends Fillable
{

    /*
     * GETTERS & SETTERS
     */
    public function getId()
    {

        return $this->id;
    }

    public function getName()
    {

        return $this->name;
    }
}
