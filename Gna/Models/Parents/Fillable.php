<?php

namespace Gna\Models\Parents;


class Fillable
{

    public function __construct($data = [])
    {

        if (!empty($data)) {
            $this->fill($data);
        }
    }

    /**
     * Emplenar l'objecte amb les propietats que ens envien
     *
     * @param array $data
     */
    public function fill($data = [])
    {

        foreach ($data as $attrName => $attrValue) {
            $this->{$attrName} = $attrValue;
        }
    }
}