<?php

namespace Gna\Models;

use Gna\Models\Parents\Fillable;
use Gna\Repositories\AuthorsRepository;
use Gna\Repositories\PublishersRepository;

class Book extends Fillable
{

    private $publisher;
    private $author;

    public function __construct($data)
    {

        parent::__construct($data);

    }

    /*
     * GETTERS & SETTERS
     */

    public function getId()
    {

        return $this->id;
    }

    public function getName()
    {

        return $this->name;
    }

    public function getPrice()
    {

        return $this->price;
    }

    public function getPriceWithTaxes()
    {

        return $this->price * 1.21;
    }

    public function getAuthorId()
    {

        return $this->author_id;
    }

    public function getPublisherId()
    {

        return $this->publisher_id;
    }


    /*
     * RELATIONS
     */


    public function author()
    {

        if (is_null($this->author)) {
            $authorsRepository = new AuthorsRepository();
            $this->author = $authorsRepository->find($this->getAuthorId());
        }

        return $this->author;
    }

    public function publisher()
    {

        if (is_null($this->publisher)) {
            $publishersRepository = new PublishersRepository();
            $this->publisher = $publishersRepository->find($this->getPublisherId());
        }

        return $this->publisher;

    }



}
