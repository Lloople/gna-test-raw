<?php

namespace Gna\Models;

use Gna\Models\Parents\Fillable;
use Gna\Repositories\BooksRepository;

class Publisher extends Fillable
{

    private $books;

    public function __construct($data)
    {

        parent::__construct($data);

    }

    /*
     * GETTERS & SETTERS
     */

    public function getName()
    {

        return $this->name;
    }

    public function getId()
    {

        return $this->id;
    }


    /*
     * RELATIONS
     */

    public function books()
    {

        if (is_null($this->books)) {
            $booksRepository = new BooksRepository();
            $this->books = $booksRepository->getBooksByPublisherId($this->getId());
        }

        return $this->books;
    }

}
