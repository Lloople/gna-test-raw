<?php include('Gna/Views/layouts/header.php'); ?>
    <h1>Publishers results</h1>

    <form method="GET" action="/publishers/list">
        <div class="form-control">
            <label for="author">Author (ID or Name):</label>
            <input type="text" value="<?= $inputAuthor ?>" name="author">
        </div>
        <div class="form-control">
            <label for="name">Publisher's name:</label>
        </div>

        <div class="form-control">
            <input type="text" value="<?= $inputName ?>" name="name">
        </div>
        <div class="form-control">
            <button class="button-flat">SEARCH</button>

            <?php if (!is_null($inputName) || !is_null($inputAuthor)): ?>
                <a class="button-flat" href="/publishers/list">CLEAR</a>
            <?php endif; ?>
        </div>
        <br><br>
    </form>
<?php if (count($publishers) > 0): ?>
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($publishers as $publisher): ?>
            <tr>
                <td><?= $publisher->getId() ?></td>
                <td><?= $publisher->getName() ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h2>No results found</h2>
<?php endif; ?>

<?php include('Gna/Views/layouts/footer.php'); ?>