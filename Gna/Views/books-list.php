<?php include('Gna/Views/layouts/header.php'); ?>

<h1>Books results</h1>

<form method="GET" action="/books/list">
    <div class="form-control">
        <label for="author">Author (ID or Name):</label>
        <input type="text" value="<?= $inputAuthor ?>" name="author">
    </div>

    <div class="form-control">
        <label for="name">Book's Name:</label>
        <input type="text" value="<?= $inputName ?>" name="name">
    </div>

    <div class="form-control">
        <label for="name">Max. price:</label>
        <input type="number" min="0" step="0.1" value="<?= $inputPrice ?>" name="price">
    </div>

    <div class="form-control">
        <label for="">Order by author</label>
        <input type="radio" value="asc" id="order_by_author_asc"
               name="order_by_author" <?= $orderByAuthor == 'asc' ? 'checked' : '' ?>>
        <label for="order_by_author_asc">ASC</label>
        <input type="radio" value="desc" id="order_by_author_desc"
               name="order_by_author" <?= $orderByAuthor == 'desc' ? 'checked' : '' ?>>
        <label for="order_by_author_desc">DESC</label>
    </div>

    <div class="form-control">
        <button class="button-flat">SEARCH</button>
        <?php if (!is_null($inputPrice) || !is_null($inputAuthor) || !is_null($inputName)): ?>
            <a class="button-flat" href="/books/list">CLEAR</a>
        <?php endif; ?>
    </div>
    <br>
    <br>
</form>
<?php if (count($books) > 0): ?>
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Author</th>
            <th>Publisher</th>
            <th>Price</th>
            <th>Price + Taxes</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $totalPrice = 0;
        $totalPriceWithTaxes = 0;
        foreach ($books as $book):
            $totalPrice += $book->getPrice();
            $totalPriceWithTaxes += $book->getPriceWithTaxes();
            ?>
            <tr>
                <td><?= $book->getId() ?></td>
                <td><?= $book->getName() ?></td>
                <td><?= $book->author()->getName() ?></td>
                <td><?= $book->publisher()->getName() ?></td>
                <td><?= $book->getPrice() ?></td>
                <td><?= $book->getPriceWithTaxes() ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="3">&nbsp;</td>
            <th>Total:</th>
            <th><?= $totalPrice ?></th>
            <th><?= $totalPriceWithTaxes ?></th>
        </tr>
        </tbody>
    </table>
<?php else: ?>
    <h2>No results found</h2>
<?php endif; ?>

<?php include('Gna/Views/layouts/footer.php'); ?>
