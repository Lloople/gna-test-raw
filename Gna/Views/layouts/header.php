<?php
$menuEntries = [
    [
        'title' => 'List of books',
        'path'  => '/books/list',
    ],
    [
        'title' => 'List of publishers',
        'path'  => '/publishers/list',
    ],
];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta author="David Llop">
    <title>Prova GNA</title>
    <link href="/resources/css/app.css" rel="stylesheet">
</head>
<body class="noise">
<div class="header-top">
    <div class="menu">
        <?php foreach ($menuEntries as $menuEntry): ?>
            <a class="<?= $menuEntry['path'] == REQUEST_PATH ? 'active' : '' ?>"
               href="<?= $menuEntry['path'] ?>"><?= $menuEntry['title'] ?></a> |
        <?php endforeach; ?>
    </div>
</div>
<div class="container">
    <br>
