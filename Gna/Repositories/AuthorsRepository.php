<?php

namespace Gna\Repositories;

use Gna\Models\Author;

class AuthorsRepository extends DataRepository
{

    protected $class = Author::class;
    protected $table = 'authors';

}