<?php

namespace Gna\Repositories;

use Gna\Models\Book;

class BooksRepository extends DataRepository
{

    protected $class = Book::class;
    protected $table = 'books';

    /*
     * Ordenar els llibres a partir del nom del autor relacionat
     */
    public function orderBooksByAuthor($books, $orderBy)
    {

        usort($books, function ($a, $b) use ($orderBy) {

            if ($orderBy == 'asc') {
                return strcmp($a->author()->getName(), $b->author()->getName());
            } else {
                return strcmp($b->author()->getName(), $a->author()->getName());
            }
        });

        return $books;
    }

    /*
     * Recollir tots els llibres, per� retornar nom�s els d'una editorial en concret
     */
    public function getBooksByPublisherId($publisherId)
    {

        $filteredBooks = array_filter($this->getBooksByAuthorPriceName(), function ($b) use ($publisherId) {

            return $b->getPublisherId() == $publisherId;
        });

        return $filteredBooks;
    }

    /*
     * Si el cridem sense parametres, ens retorna tots els llibres
     * Podem: Filtrar per autor (ID o NAME)
     *        Filtrar per preu igual o inferior a
     *        Filtrar per titol del llibre
     */
    public function getBooksByAuthorPriceName($author = null, $price = null, $name = null)
    {

        $books = $this->getDataFromTable();
        if (!is_null($author)) {
            $books = $this->filterByAuthor($books, $author);
        }
        if (!is_null($price)) {
            $books = $this->filterByPrice($books, $price);
        }
        if (!is_null($name)) {
            $books = $this->filterByName($books, $name);
        }

        return $books;
    }

    public function filterByAuthor($books, $author)
    {

        if (is_numeric($author)) {
            // Si �s numeric, comprovem per el ID
            $filteredBooks = array_filter($books, function ($b) use ($author) {

                return $b->getAuthorId() == $author;
            });
        } else {
            // Si no, mirem si el nom de l'autor cont� aquest text
            $filteredBooks = array_filter($books, function ($b) use ($author) {

                return strpos(strtolower($b->author()->getName()), strtolower($author)) !== false;
            });
        }

        return $filteredBooks;
    }

    public function filterByPrice($books, $price)
    {

        $filteredBooks = array_filter($books, function ($b) use ($price) {

            return $b->getPrice() <= $price;
        });

        return $filteredBooks;
    }

    public function filterByName($books, $name)
    {

        $filteredBooks = array_filter($books, function ($b) use ($name) {

            return (strpos(strtolower($b->getName()), strtolower($name)) !== false);
        });


        return $filteredBooks;
    }

}