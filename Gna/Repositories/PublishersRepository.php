<?php

namespace Gna\Repositories;

use Gna\Models\Publisher;

class PublishersRepository extends DataRepository
{

    protected $class = Publisher::class;
    protected $table = 'publishers';

    /**
     * Recollir totes les editorials coincidents amb el criteri de cerca
     *
     * @param null $author
     * @param null $name
     *
     * @return array
     */
    public function getPublishersByAuthorName($author = null, $name = null)
    {

        $publishers = $this->getDataFromTable();
        if (!is_null($author)) {
            $publishers = $this->filterByAuthor($publishers, $author);
        }

        if (!is_null($name)) {
            $publishers = $this->filterByName($publishers, $name);
        }

        return $publishers;
    }

    /**
     * Filtrar per autor, tant pel nom com per el ID
     *
     * @param $publishers
     * @param $author
     *
     * @return array
     */
    public function filterByAuthor($publishers, $author)
    {

        $filteredPublishers = array_filter($publishers, function ($p) use ($author) {

            $filteredPublisherBooks = array_filter($p->books(), function ($b) use ($author) {

                if (is_numeric($author)) {
                    return $b->getAuthorId() == $author;
                } else {
                    return strpos(strtolower($b->author()->getName()), strtolower($author)) !== false;
                }
            });

            return count($filteredPublisherBooks) > 0;
        });

        return $filteredPublishers;
    }

    /**
     * Filtrar pel nom de la editorial
     *
     * @param $publishers
     * @param $name
     *
     * @return array
     */
    public function filterByName($publishers, $name)
    {

        $filteredPublishers = array_filter($publishers, function ($p) use ($name) {

            return (strpos(strtolower($p->getName()), strtolower($name)) !== false);
        });


        return $filteredPublishers;
    }

}