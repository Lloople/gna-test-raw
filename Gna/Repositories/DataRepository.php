<?php

namespace Gna\Repositories;


use Gna\Helpers\Database;

class DataRepository
{

    protected $class;
    protected $table;
    private $db;

    public function __construct()
    {

        $this->db = new Database();

    }

    public function find($id)
    {

        $dbResults = $this->getDataFromTable();

        $result = array_values(array_filter($dbResults, function ($a) use ($id) {

            return $a->getId() == $id;
        }));

        if (count($result) == 1) {
            return $result[0];
        }

        return null;
    }

    public function getDataFromTable()
    {

        $results = [];
        $dbResults = $this->db()->get($this->table);
        foreach ($dbResults as $dbResult) {
            $result = new $this->class($dbResult);
            $results[] = $result;
        }

        return $results;
    }

    public function db()
    {

        return $this->db;
    }

}